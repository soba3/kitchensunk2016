// navigation scripts 
// for mobile menu
// and submenu
var Navigation = (function(){
	
	var s,
      vars;


	var mobilenav = {
		settings: {
			btnClass: '.menu-button',
			btnHtml: '<button class="menu-button" data-target="#mainnav" aria-expanded="false" aria-controls="navbar"><svg class="menu_svg"><use xlink:href="#icon-menu"></use></svg></button>',
			containerSel: '.primary-nav',
			menuLevel1: '.level-1'
		},

		vars: {
			$mobileMenu: undefined
		},

		init: function(){

			s = mobilenav.settings;
      vars = mobilenav.vars;

      $(s.containerSel).addClass('mobile-nav');
			mobilenav.addMenuButton();
			mobilenav.closeMenu($(s.containerSel));

			

		},

		addMenuButton: function(){

			var $menu = $(s.containerSel);

			//if no button, 
			if (!$(s.btnClass).length){
				// add button markup
				$(s.containerSel).before(s.btnHtml);

			} 

			$(s.btnClass).on('click', function(){

				var $this = $(this);
				//simple togggle:
				$menu.toggleClass('menu-open');

				$this.toggleClass('button-active');

				//update aria attribute
				if ($this.attr('aria-expanded') === 'false'){
					$this.attr('aria-expanded', 'true');
				} else {
					$this.attr('aria-expanded', 'false');
				}

			});
			
		},

		closeMenu: function(menu){

			$('body').on('click', function(e){

				//console.log('from closeMenu: ');
				if (!$(e.target).closest('.level-1').length 
							&& !$(e.target).is('.level-1').length 
									&& !$(e.target).is('.menu-button')) {
										menu.removeClass('menu-open');	
										$(s.btnClass).removeClass('button-active');
									}
				

			});
				
		},

		

	}

	//Submenu scripts

	var submenu = {

		//secondary nav, show/hide
		init: function(){
			//get all level 1 elelemnts and add hook if has children
			var menuItems = $('.level-1').children('li');
			//var menuItems = document.querySelectorAll('.level-1 > li');

			for (var i = menuItems.length - 1; i >= 0; i--) {
				var $item = $(menuItems[i]),
						$submenu = $item.find('.toc');

				//add a hook to items with submenu
				if ($submenu.length){
					$item.addClass('has-children');
					$item.children('a').addClass('parent');
					$submenu.addClass('subitem-' + i);
				};
			}

			submenu.subMenuOpen();

		},

		subMenuOpen: function(){

			var isExpanded = false,
					isMobile = window.innerWidth <= 640;

			// only for menu items that have submenus:
			$('.has-children').on('click', function(e){
				
				var $secondary = $(this).children('.toc');

				//close all submenus before opening another
				if ($('.submenu-open').length){

					$('.submenu-open').removeClass('submenu-open');

				}			


				// if (isMobile){
				// 	if ($(e.target).hasClass('parent')){
				// 		return;
				// 	}
				// }
					

				// will not follow link when opening submenu
				//BUT needs to follow link when clicked while submenu is open
				if ($(e.target).hasClass('parent')) {
						$secondary.toggleClass('submenu-open');						
						e.preventDefault();
						
				} 
				

				submenu.subMenuClose();

				// check if 
				// if ( $(e.target).hasClass('parent')){
				// 	$(e.target).addClass('expanded-flag');
				// 	$secondary.toggleClass('submenu-open');
				// 	e.preventDefault();


				// 	submenu.subMenuClose();
				// } 


			});
		},

		subMenuClose: function(menu){

			$('body').on('click', function(e){
				var $evtarget = $(e.target);

				//close when clicking away
				if (!$evtarget.hasClass('parent') ){
					$('.submenu-open').removeClass('submenu-open');						

				} 
				
				
				

			});
		}


	}



	return {
		init: mobilenav.init,

		submenu: submenu.init

	}

})();


//DOM ready:
(function($) {

	Navigation.init();

	Navigation.submenu();

})(jQuery);




