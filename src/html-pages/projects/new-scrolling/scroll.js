// this function self-initializes and attaches an event listener to the root element
var smoothScroll = (function(root){
    //keep track of the target element between scrolling function and transitionend callback
    var targetElement;

    // called when the CSS transition finishes
    root.addEventListener('transitionend', function(e){
      /// remove transition and transform
      root.style['transition'] = '';
      root.style['transform'] = '';
      //do the scrolling
      targetElement.scrollIntoView();
    });


    // this function is returned as the smoothScroll function
    return function(element, time){
      // get the top position of target element
      var offset = element.offsetTop - (window.scrollY||document.documentElement.scrollTop||root.scrollTop);
      // if the element is very low it can't get scrolled up to the top of the window
      //Math.min(num1, num2, ...) returns the smallest of zero or more numbers.
      offset = Math.min( offset, root.offsetHeight - document.documentElement.clientHeight );
      // save reference to the target element for callback
      targetElement = element;

      // set CSS properties with passed value for time, and calculated offset value
      root.style['transition'] = 'transform '+time;
      // this fakes the scrolling animation by animating transform on the element
      root.style['transform'] = 'translateY(' + offset * -1 +'px)';
    }
}(document.body)); //calling the function with body as argument (= root)

[].slice.call( document.getElementsByTagName('a') ).forEach(
    function(link){
        link.addEventListener( 'click', function(e){
        var targetElement = document.getElementById( e.target.href.replace(/[^#]*#/,'') );
        smoothScroll( targetElement, '500ms' );
       e.preventDefault(); 
    })
});