# Project - data graphs


## The challenge:

Dynamic graph with exchangeable data (via JSON or JS data in JSON format).

Scenario: Compare Currency exchange service for certain currencies and certain amounts, with competitors.

- try to use pure js (resorted to jquery!)

- load/read JSON data
	`JSON.parse('string')` (actually, now using jquery promise!)
- amount to exchange is selectable from a dropdown
- currecny type is also selectable
- graph should be animated
- calculate and show the differential between the showcase service and competitors (i.e. savings)
- number of bars depends on entries in database
- able to switch between horizontal and vertical bar graph styles (for respnsiveness)
- just one graph per page for now.

## Current script execution order 

`loadJSON` (using jquery promise)
	call	`getSelectValues` (readds selected vals, listens for changes)
					call `setupGraphData` (onchange)
	call	`setupGraphData` (takes the selected dataset, modify and add to data, sort the data + determine the max)
				call `createBars` (creates array of bars markup from sorted data)





## Room for improvement (in terms of performance)
- transform: `scale(x,y)` with `transform-origin`   to avoid repaint?
	- but would need to think re responsive graph - what value to scale to??
- because the select amounts are fixed, could do all the rates x amount calcs and store them for quicker access before the rest of the app runs/ user changes values. Might be better than doing the math each time 
	- may be do the defaults for init load, and run the calcs for the rest in idle time?

- currently all graph bars get re-written on change.
	could be better to: 
		- on load: collect default data, build the graph in the DOM, 
							 run the animation
		- on init: load the rest of the data and prepare for use
		- on change: collect/update the selected dataset
								 sort into best rate order
								 iterate the dom grapg bars and update 
								 run the animation
		
