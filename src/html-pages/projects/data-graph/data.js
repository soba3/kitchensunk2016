var rates = [
  {
    "amount": 10000,
    "currency": "EUR",
    "bank": "Natwest",
    "rate": 1.231
  },
  {
    "amount": 10000,
    "currency": "EUR",
    "bank": "Telegraph",
    "rate": 1.2694
  },
  {
    "amount": 10000,
    "currency": "EUR",
    "bank": "HSBC",
    "rate": 1.2508
  },
  {
    "amount": 10000,
    "currency": "EUR",
    "bank": "Barclays",
    "rate": 1.2559
  },

  {
    "amount": 25000,
    "currency": "EUR",
    "bank": "Natwest",
    "rate": 1.231
  },
  {
    "amount": 25000,
    "currency": "EUR",
    "bank": "Telegraph",
    "rate": 1.2732
  },
  {
    "amount": 25000,
    "currency": "EUR",
    "bank": "HSBC",
    "rate": 1.2584
  },
  {
    "amount": 25000,
    "currency": "EUR",
    "bank": "Barclays",
    "rate": 1.2559
  },

  {
    "amount": 100000,
    "currency": "EUR",
    "bank": "Natwest",
    "rate": 1.231
  },
  {
    "amount": 100000,
    "currency": "EUR",
    "bank": "Telegraph",
    "rate": 1.2761
  },
  {
    "amount": 100000,
    "currency": "EUR",
    "bank": "Lloyds",
    "rate": 1.2586
  },
  {
    "amount": 100000,
    "currency": "EUR",
    "bank": "Barclays",
    "rate": 1.2559
  },

  {
    "amount": 10000,
    "currency": "USD",
    "bank": "Natwest",
    "rate": 1.3449
  },
  {
    "amount": 10000,
    "currency": "USD",
    "bank": "Telegraph",
    "rate": 1.3946
  },
  {
    "amount": 10000,
    "currency": "USD",
    "bank": "HSBC",
    "rate": 1.375
  },
  {
    "amount": 10000,
    "currency": "USD",
    "bank": "Barclays",
    "rate": 1.377
  },

  {
    "amount": 25000,
    "currency": "USD",
    "bank": "Natwest",
    "rate": 1.3449
  },
  {
    "amount": 25000,
    "currency": "USD",
    "bank": "Telegraph",
    "rate": 1.3996
  },
  {
    "amount": 25000,
    "currency": "USD",
    "bank": "Barclays",
    "rate": 1.377
  },
  {
    "amount": 25000,
    "currency": "USD",
    "bank": "RBS",
    "rate": 1.3783
  },

  {
    "amount": 100000,
    "currency": "USD",
    "bank": "Natwest",
    "rate": 1.3449
  },
  {
    "amount": 100000,
    "currency": "USD",
    "bank": "Telegraph",
    "rate": 1.4046
  },
  {
    "amount": 100000,
    "currency": "USD",
    "bank": "Barclays",
    "rate": 1.377
  },
  {
    "amount": 100000,
    "currency": "USD",
    "bank": "RBS",
    "rate": 1.3783
  }
]