// Still unsure which module pattern best suits

// the stacked locally scoped object literal
// maybe change to revealing module?


var DataGraphs = (function() {

  var s,
      vars;

  var moduleObj = {

    settings: {
      isHorizontal: true, //horizontal graph
      graphWrapper: document.getElementById('dataGraph'), //graph container in the DOM
      ggGraph: '<div class="gg"></div>', //generated graph inner container
      ggCol: '<div class="gg_col"></div>', //generated column containing bar
      ggCol: '<div class="gg_col-inner"></div>', //generated inner column containing bar
      ggBar: '<div class="gg_bar"></div>' //generated bar
    },

    vars: {
      dataLoaded: false,
      loadedData: undefined,
      augmentedData: undefined, //store the data returned via setupGraphData?
      selectedVals: {},
      selectForm: document.getElementById('selectForm'),
      select1: document.getElementById('amount'),
      select2: document.getElementById('currency'),
      orientationProps: {},
      max: 0 //store highest num value in looped results
    },

    init: function(){
      //public method
      s = moduleObj.settings;
      vars = moduleObj.vars;

      //console.log(this);//refers to Datagraphs which returns public methods
      // moduleObj.parseJSON();

      moduleObj.loadJSON();

      moduleObj.getSelectValues(); //get the default selection

      //set orientation of graph:
      if(s.isHorizontal){
        vars.orientationProps = {
          graphClass: 'horizontal',
          styleProp: 'width: '
        };
      } else {
        vars.orientationProps = {
          graphClass: 'vertical',
          styleProp: 'height: '
        };
      }



    },

    // UTILITY functions:
    //add comma(s) to string numbers in 000's 
    numsWithCommas: function(x){
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    },

    //set up currency symbols to be added to amounts in graph
    currencySymbols: function(currency){
      var currSymbol;
      if (currency === 'EUR'){
        currSymbol = '&euro;';
      } else if (currency === 'USD'){
        currSymbol = '$';
      } else {
        currSymbol = '&pound;';
      }
      return currSymbol;
    },

    // load JSON:
    loadJSON: function(callback){
      //resorted to jquery!
      var promise = $.getJSON('data.json');

      promise.done(function(data) {
        console.log('data loaded: ', data[21].bank);
        vars.dataLoaded = true;
        //moduleObj.useData(data, true);
        vars.loadedData = data;

        // all processes that need the database to have loaded go here:
        vars.selectForm.addEventListener("change", function(){
          vars.selectedVals.amount = parseInt(vars.select1.value, 10);
          vars.selectedVals.currency = vars.select2.value;

          console.log('onchange: ', vars.selectedVals);

          //run the output function
          //console.log(dataLoaded, loadedData[0].bank);//OK

          moduleObj.setupGraphData(vars.selectedVals);

        });

        console.log('init: selectedVals = ', vars.selectedVals);

        //run the output function

        moduleObj.setupGraphData(vars.selectedVals);


      });

      promise.fail(function() {
        console.log('failed to load data')
      });
    },

    
    // get values from select:
    getSelectValues: function(){
      vars.selectedVals = {
        amount: parseInt(vars.select1.value, 10),
        currency: vars.select2.value
      };
    },

    
    //json data may not always be in a convenient order
    //host data should be top of the pile
    // runs inside setupGraphData function, on array of selected data
    moveHostTop: function(selectedData){

      Array.prototype.move = function (old_index, new_index) {
          if (new_index >= this.length) {
              var k = new_index - this.length;
              while ((k--) + 1) {
                  this.push(undefined);
              }
          }
          this.splice(new_index, 0, this.splice(old_index, 1)[0]);
          return this; // for testing purposes
      };
      
      
      for (var i = selectedData.length - 1; i >= 0; i--) {

        if (selectedData[i].bank === 'Telegraph'){
          selectedData.move(i, 0);

        }
      }

      return selectedData;
    },

    // trawl the selected data, sort and add properties
    // called from inside promise in loadJSON
    setupGraphData: function(selected){
      //console.log(selected, loadedData);//OK

      var data = vars.loadedData,
          dataArray = [],
          hostResult,
          difference,
          currSymbol = '';

      // set up currency symbols to be added to text results
      currSymbol = moduleObj.currencySymbols(selected.currency);

      //iterate data object and add the appropriate entries to an array
      //[may be better to covert the whole object literal into an array to make it easier to sort/change order of items?!]
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          //console.log(data[key].amount, selected.amount);
          if (data[key].amount === selected.amount){            
            if (data[key].currency === selected.currency){
              //calculate rate * amount 
              var result = Math.round((data[key].amount * data[key].rate)*100) / 100;

              //if current iteration is for the host programme, store result for later use
              if (data[key].bank === 'Telegraph'){
                hostResult = result;
                //console.log('Host result: ', result)
              }

              //defer calc of diffs between host and competitors till after the loop is finished 
              //(host entry is not neccessarily the first!)
              
              //get the max value from result 
              //...use to calculate percentage later(for bar width/height)
              if (result > vars.max){
                vars.max = result;
              }

              // ...and add the results into to the data object:
              data[key].resultAmt = result;// value for graph bar
              data[key].resultAmtStr = currSymbol + moduleObj.numsWithCommas(result); //text for screen
              data[key].diffWithHost = null;

              //console.log(data[key]);

              //then:
              dataArray.push(data[key]);
            }
            
          }
        }
      }

      //change order of items in the resulting array, so the host is top:
      dataArray = moduleObj.moveHostTop(dataArray);
      //store the new properties and values 
      vars.loadedData = data;

      moduleObj.createBars(dataArray, hostResult);

    },


    // iterate completed data returned from setupGraphData
      //- fill in missing diffWithHost
      //- build the mark up for each graph bar
    // called from setupGraphData
    createBars: function(dataArr, hostRes){
      var barsArray = [];

      var selectedAmount = vars.selectedVals.amount;
          offsetAmount = selectedAmount * 1.2, //to create visible diff between graph bars
          max = vars.max - offsetAmount;

      for (var i = dataArr.length - 1; i >= 0; i--) {  

        //store diffs between host and competitors to each entry   
        dataArr[i].diffWithHost = dataArr[i].resultAmt - hostRes;  
        
        var barSize = (dataArr[i].resultAmt).toFixed(0);

        //as the numbers are too close to show significant difference...
        barSize = barSize - offsetAmount;
        console.log('modified for contrast barSize: ', barSize); 
        console.log((selectedAmount / 1000)*2);

        //set value out of 100 to use for bar height/width
        var percent = (((barSize / (max))*100));

        //want the graph to show proportionate difference in size for different selected amounts
        //e.g. bars for 100,000 should look bigger than for 25,000
        var percentPlus = (percent + ((selectedAmount / 1000)*1.2)).toFixed(0) ;
        //may be stick to the percentage plan, and adjust overall graph hight/width according to selected amount?    

        //create each bar markup while in the loop:
        var bar = '<div class="gg_bar bar_' + i + '" style="'; 
            bar += vars.orientationProps.styleProp + percentPlus; 
            bar += 'px;" data-bank="' + dataArr[i].bank; 
            bar += '"></div>' ;

        //add to barsArray 
        //[using unshift rather than push because of the for-loop iteration]
        barsArray.unshift(bar);

      }

      //pass the requested data to create graph 
      moduleObj.buildGraph(barsArray, dataArr);

      //reset max
      vars.max = 0;

    },


    //build bar columns markup with data (as string)
    //called from createBars
    //using jquery now
    buildGraph: function(strArray, data){

      // setup for graph to be gen'd:
      s.graphWrapper.innerHTML = s.ggGraph;

      var $graphInner = $('.gg');

      //set graph orientation
      $graphInner.addClass(vars.orientationProps.graphClass);

      //iterate selected data set and create inner column with bar :
      for (var i = 0; i < strArray.length; i++) {
        //strArray[i];

        var barObj = {};
        barObj.col_inner = $('<div class="gg_col-inner"></div>').appendTo($('.gg'));
        barObj.bar = $(strArray[i]).appendTo(barObj.col_inner);
        $('<span class="bank">' + data[i].bank + '</span>').prependTo(barObj.col_inner);
        $('<span class="amount">' + data[i].resultAmtStr + '</span>').appendTo(barObj.col_inner);
        $('<span class="difference">' + data[i].diffWithHost + '</span>').appendTo(barObj.col_inner);
      }

      
      

    },


    // graph bars start off with width/height 0
    // function called from buildGraph
    displayBars: function(bars, i){

      if (i < bars.length) { //issues with $.each, so looping another way

        // Add transition properties and set height via CSS
        $(bars[i].bar).css({'height': bars[i].height, 'transition': 'all 0.8s ease-out'});
        
        // Wait the specified time then run the displayGraph() function again for the next bar
        barTimer = setTimeout(function() {
          i++;        
          Graphs.displayBars(bars, i);
        }, 100);
      }
      
    },

    // set graph bars height/width to 0, ready to animate
    
    resetBars: function(bars, barTimer, graphTimer){
      //Set bar height to 0 and clear all transitions
      //console.log(bars);
      $.each(bars, function(i) {
        $(bars[i].bar).stop().css({'height': 0, 'transition': 'none'});
        //console.log($(bars[i].bar));
      });
      
      // Clear timers
      clearTimeout(barTimer);
      clearTimeout(graphTimer);
      
      // Restart timer    
      graphTimer = setTimeout(function() {    
        moduleObj.displayBars(bars, 0);
      }, 200);
    },





  };



  //return moduleObj;
  return {
    init: moduleObj.init,
    //getSelectValues: moduleObj.getSelectValues
  }


})();

//console.log(DataGraphs);
DataGraphs.init();

//DataGraphs.getSelectValues(); 

console.log();











