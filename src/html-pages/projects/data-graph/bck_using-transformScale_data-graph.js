// Still unsure which module pattern best suits

// the stacked locally scoped object literal
// maybe change to revealing module?


var DataGraphs = (function() {

  var s,
      vars;

  var moduleObj = {

    settings: {
      dataUrl: 'data.json',
      isHorizontal: true, //horizontal graph
      graphWrapper: document.getElementById('dataGraph'), //graph container in the DOM
      ggGraph: '<div class="gg"></div>', //generated graph inner container
    },

    vars: {
      loadedData: undefined,
      currSelectData: undefined, //store the selected data (modified and sorted )
      selectedVals: {},
      selectForm: document.getElementById('selectForm'),
      select1: document.getElementById('amount'),
      select2: document.getElementById('currency'),
      orientationProps: {},
      max: 0 //store highest num value in looped results    
    },

    init: function(){
      //public method
      s = moduleObj.settings;
      vars = moduleObj.vars;

      //console.log(this);//refers to Datagraphs which returns public methods
      // moduleObj.parseJSON();

      moduleObj.loadJSON(s.dataUrl);

      //moduleObj.getSelectValues(); //get the default selection

      //set orientation of graph:
      if(s.isHorizontal){
        vars.orientationProps = {
          graphClass: 'horizontal',
          styleProp: 'width'
        };
      } else {
        vars.orientationProps = {
          graphClass: 'vertical',
          styleProp: 'height'
        };
      }

      

    },


    // load JSON:
    loadJSON: function(dataPath){
      //resorted to jquery!
      var promise = $.getJSON(dataPath);

      promise.done(function(data) {
        //console.log('data loaded: ', data[21].bank);
        //moduleObj.useData(data, true);
        vars.loadedData = data;

        // all processes that need the database to have loaded go here:
        moduleObj.getSelectValues();
        //console.log('init: selectedVals = ', vars.selectedVals);        

        //run the setup function:
        moduleObj.setupGraphData(vars.selectedVals);

      });

      promise.fail(function() {
        console.warn('failed to load data');
      });
    },


    
    // get values from select 
    // called from inside promise.done in loadJSON
    getSelectValues: function(){
      vars.selectedVals = {
        amount: parseInt(vars.select1.value, 10),
        currency: vars.select2.value
      };

      //Listen to select onchange
      vars.selectForm.addEventListener("change", function(){
        vars.selectedVals.amount = parseInt(vars.select1.value, 10);
        vars.selectedVals.currency = vars.select2.value;

        console.log('onchange: ', vars.selectedVals);

        //run the output function
        moduleObj.setupGraphData(vars.selectedVals);

      });
    },

    
    // UTILITY functions:
    //add comma(s) to string numbers in 000's 
    numsWithCommas: function(x){
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    },

    //set up currency symbols to be added to amounts in graph
    currencySymbols: function(currency){
      var currSymbol;
      if (currency === 'EUR'){
        currSymbol = '&euro;';
      } else if (currency === 'USD'){
        currSymbol = '$';
      } else {
        currSymbol = '&pound;';
      }
      return currSymbol;
    },

    // trawl the selected data, sort and add properties
    // called from inside promise.done in loadJSON
    setupGraphData: function(selected){
      //console.log(selected, loadedData);//OK

      var data = vars.loadedData,
          dataArray = [],
          bestRate,
          difference,
          currSymbol = '';

      // set up currency symbols to be added to text results
      currSymbol = moduleObj.currencySymbols(selected.currency);

      //iterate data object and add the appropriate entries to an array
      //could have used object.key()!
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          
          if (data[key].amount === selected.amount){            
            if (data[key].currency === selected.currency){

              //calculate rate * amount 
              var result = Math.round((data[key].amount * data[key].rate)*100) / 100;

              //defer calc of diffs between host and competitors till after the loop is finished 
              
              //get the max value from result 
              //...use to calculate percentage later(for bar width/height)
              if (result > vars.max){
                vars.max = result;
              }

              // ...and add the results into to the data object:
              data[key].resultAmt = result;// value for graph bar
              data[key].resultAmtStr = currSymbol + moduleObj.numsWithCommas(result); //text for screen
              data[key].diffWithHost = null;

              //console.log(data[key]);

              //then:
              dataArray.push(data[key]);
            }
            
          }
        }
      }


      //sort order of items from best to worst:
      dataArray = moduleObj.sortData(dataArray);

      //store the new properties and values 
      vars.loadedData = data;
      vars.currSelectData = dataArray;

      moduleObj.createBars(currSymbol);
      //SAVE TO GLOBAL VAR console.log(dataArray);

    },

    //Sort data from best (highest amount value -> lowest)
    // runs inside setupGraphData function, on array of selected data
    sortData: function(selectedData){
      //sort by amount values
      // using .keys() to convert to array, then run .map() to return new array
      var sortedData = Object.keys(selectedData).sort(function(a, b) { 
        return selectedData[b].resultAmt - selectedData[a].resultAmt;
      }).map(function(sortedKey) {
        return selectedData[sortedKey];
      });

      //console.log('sorted data: ',sortedData);
      return sortedData;

    },

    //get the difference with best rate
    //call this from createBars
    getDifference: function(currItem, bestRes, symbol){
      var diff;

      diff = (currItem.resultAmt - bestRes).toString(); 
        if (diff.indexOf('-') === 0){
          diff = '-' + symbol + diff.substr(1); //remove the minus
        } 
        //console.log(diff);
        //unless the difference is 0, add the new string value to data
        currItem.diffWithHost = diff !== '0' ? diff : '&nbsp;';
    },

    
    //create the graph bar element from sorted/prep'd data
    // called from setupGraphData
    createBars: function(symbol){
      ////SAVE TO GLOBAL VAR console.log(dataArr);
      var dataArray = vars.currSelectData,
          barsArray = [];

      var selectedAmount = vars.selectedVals.amount;
          offsetAmount = selectedAmount * 1.2, //to create visible diff between graph bars
          bestResult = vars.max,
          max = vars.max - offsetAmount,
          diff = undefined,
          symbol = symbol,
          containerWidth = $('.calculator').width();
      console.log(containerWidth);

      // iterate completed data returned from setupGraphData
      // - fill in missing diffWithHost
      // - build the mark up for each graph bar
      for (var i = dataArray.length - 1; i >= 0; i--) {  

        //store difference with the best rate 
        moduleObj.getDifference(dataArray[i], bestResult, symbol);   


        moduleObj.calculateBarSize(containerWidth);


        var barSize = (dataArray[i].resultAmt).toFixed(0);

        //to exaggerate the difference between each bank (figuress too close to show significant difference...)
        barSize = barSize - offsetAmount;
        //console.log('modified for contrast barSize: ', barSize); 
        //console.log((selectedAmount / 1000)*2);

        //set value out of 100 to use for bar height/width
        var percent = (((barSize / (max))*100));

        //console.log(percent);

        //want the graph to show proportionate difference in size for different selected amounts
        //e.g. bars for 100,000 should look bigger than for 25,000
        var percentPlus = (percent + ((selectedAmount / 1000)*1.2)).toFixed(0) ;

        //may be stick to the percentage plan, and adjust overall graph hight/width according to selected amount? 


        //create each bar markup while in the loop:
        var bar = '<div class="gg_bar bar_' + i + '"'; 
            bar += ' data-size="' + barSize + '"'; 
            bar += ' data-bank="' + dataArray[i].bank + '"></div>'; 

        //add to barsArray 
        //[using unshift rather than push because of the for-loop iteration]
        barsArray.unshift(bar);

      }

      //pass the requested data to create graph 
      moduleObj.buildGraph(barsArray);

      //SAVE TO GLOBAL VAR console.log(dataArr);

      //reset max
      vars.max = 0;


    },

    // calculate px value for graph bars to scale to
    calculateBarSize: function(container){
      //get container width
      //console.log(container);
    },


    //build bar columns markup with data (as string)
    // parameters: 
    //    strArray: array of div markup for each bar of graph
    //called from createBars
    //using jquery now
    buildGraph: function(strArray){

      var dataArray = vars.currSelectData;

      // setup for graph to be gen'd:
      s.graphWrapper.innerHTML = s.ggGraph;

      var $graphInner = $('.gg');

      //set graph orientation
      $graphInner.addClass(vars.orientationProps.graphClass);

      var graph = {};
      //iterate selected data set and create inner column with all children and add to container in DOM :
      for (var i = 0; i < strArray.length; i++) {

        graph.column = $('<div class="gg_col-inner"></div>').appendTo($graphInner);
        graph.bar = $(strArray[i]).appendTo(graph.column);
        $('<span class="bank">' + dataArray[i].bank + '</span>').prependTo(graph.column);
        $('<span class="amount">' + dataArray[i].resultAmtStr + '</span>').appendTo(graph.column);
        $('<span class="difference">' + dataArray[i].diffWithHost + '</span>').appendTo(graph.column);
      }

      //fix race condition with animation:


      // Reset graph to zero before animating:
      requestAnimationFrame(moduleObj.animateBars);

    },



    // set graph bars height/width to 0 (currently don't need to, as the whole graph gets rebuilt each time!), 
    // then animate
    //(abandoned transform: scale idea - )
    
    animateBars: function(){
      //Set bar height to 0 and clear all transitions

      var ggBars = $('.gg_bar'),
          //property = vars.orientationProps.styleProp; //could be used in ES6 {[property]: 'value'}
          oPos = '', //transform-origin
          barSize,
          scaleTo;

      oPos = s.isHorizontal ? 'left center' : 'center bottom';

      //now animate graph on setTimeout 
      //(need the delay to get around race condition with dom build script - quick fix for now)
      setTimeout(function(){
        ggBars.each(function(i){

          barSize = parseInt($(this).attr('data-size')).toFixed(0);
          scaleTo = 'scale(' + barSize + ', 1)';
          console.log(scaleTo);
          $(ggBars[i]).stop().css({'transform': scaleTo, 'transform-origin': 'left'});

          // var barSize = $(this).attr('data-size') + '%';
          // if (s.isHorizontal){
          //   $(ggBars[i]).stop().css({'width': barSize});
          // } else {
          //   $(ggBars[i]).stop().css({'height': barSize});
          // }          
        
        });
      }, 0); //http://javascript.info/tutorial/events-and-timing-depth#the-settimeout-func-0-trick

    },



  };



  //return moduleObj;
  return {
    init: moduleObj.init,
    //getSelectValues: moduleObj.getSelectValues
  }


})();


DataGraphs.init();















