// Still unsure which module pattern best suits

// the stacked locally scoped object literal
// maybe change to revealing module?


var DataGraphs = (function() {

  var s,
      selectedVals;

  var moduleObj = {
    settings: {
      useYAxis: false, //can omit the y-axis
      graphWrapper: document.getElementById('dataGraph'), //graph container in the DOM
      ggGraph: '<div class="gg"></div>', //generated graph inner container
      ggCol: '<div class="gg_col"></div>', //generated column containing bar
      ggBar: '<div class="gg_bar"></div>' //generated bar
    },

    init: function(){
      //public method
      s = moduleObj.settings;
      //console.log('foo is: ', s.foo);
      console.log(this);//refers to Datagraphs which returns public methods
      moduleObj.parseJSON();

      moduleObj.getSelectValues(); //returns selected amount and currency

    },

    // asychronously load JSON:
    loadJSON: function(callback){
      var xhttp = new XMLHttpRequest();
      var dataUrl = 'data.json';
      xhttp.overrideMimeType("application/json");
      xhttp.open('GET', dataUrl, true); // json file to load, async set to true
      xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == "200") {
          // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
          callback(xhttp.responseText);
        }
      };
      xhttp.send(null);  
    },
    parseJSON: function() {
      this.loadJSON(function(response){
        //console.log(response); //returns JSON data string
        //parse JSON string into object:
        var data = JSON.parse(response);

        //ensure data has loaded before use
        moduleObj.useData(data);
      });
      
    },

    useData: function(dataObj){

      console.log(dataObj[0].bank);
      // use values from select form
      console.log('from useData: ', selectedVals);
      //ISSUE: not updating selected from user input/onchange

      // to output corresponding data from database


      
      
      


      

      //use index for data retrieval (curr and amt)


      //build markup for graph
      moduleObj.graphsHtml(dataObj);


      //output to DOM (as data- attributes?)
      //console.log(dataObj[0].amount);

    },


    //GRAPHS MARKUP
    graphsHtml: function(data){
      //console.log('data3: ', data[3]);
      // graphWrapper: document.getElementsByClassName('dataGraph'), //graph container in the DOM
      // ggGraph: '<div class="gg"></div>', //generated graph inner container
      // ggCol: '<div class="gg_col"></div>', //generated column containing bar
      // ggBar: '<div class="gg_bar"></div>' //generated bar

      // build graph on load with defaults:

      s.graphWrapper.innerHTML = s.ggGraph;

      console.log('output from graphsHtml - for the mark up');

      var graph;

      //iterate the default data

      // for (var i = Things.length - 1; i >= 0; i--) {
      //   Things[i]
      // }

      //s.graphWrapper.innerHTML = data[0];




    },

    //GET SELECTED VALUES
    getSelectValues: function(){
      //public method
      var selectForm = document.getElementById('selectForm'),
          select1 = document.getElementById('amount'),
          select2 = document.getElementById('currency');

      //get default selected values into object literal:
      selectedVals = {
        amount: select1.value,
        currency: select2.value
      };

      //listen for user initiated changes
      selectForm.addEventListener("change", function(){
        selectedVals.amount = select1.value;
        selectedVals.currency = select2.value;

        //call function to output the corresponding data :
        //console.log(selectedVals);
        //return selectedVals;
        moduleObj.useData(selectedVals);

      });

      //call function to output the corresponding data:
      //console.log(selectedVals);
      //return selectedVals;
      moduleObj.useData(selectedVals);
    }
   




  };



  //return moduleObj;
  return {
    init: moduleObj.init,
    //getSelectValues: moduleObj.getSelectValues
  }


})();

//console.log(DataGraphs);
DataGraphs.init();

//DataGraphs.getSelectValues(); 











