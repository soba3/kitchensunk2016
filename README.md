
# DEV NOTES

## Dev set up requirements:

- [node and npm](http://nodejs.org/) (global install)
- [Grunt CLI](http://gruntjs.com/getting-started#installing-the-cli) (global install)
- [Bower](http://bower.io/#install-bower) (global install)

- no dependency on Ruby (changed to Libsass, in the form of `grunt-sass` - Libsass v3.3 is apparently fully compatible `http://sass-compatibility.github.io/`)

## Node and Bower

Assuming node already installed, `npm install` will install everything listed in `package.json`.

Assuming Bower already installed, `bower install` will install everything listed in `bower.json`.


## This project uses Grunt:

...which runs these modules (see `package.json` or `Gruntfile.js` for details):

- Sass (libsass)
- PostCSS (currently just for autoprefixer)
- Browserify (for modular javascript)
- Codekit (page builder/concatenator)
- etc (server, watch, minification and copy tasks)



## Susygrid (bower install)

import into css like any other sass file:

`@import "../../bower_components/susy/sass/susy";`

The path to the folder is relative to the file doing the importing.

## Codekit

Builds a page from components, which allows for reusable code blocks, also supports variables, but doesn't generate pages.
So there are no master templates and each page has its own kit file.


## PROJECT NOTES

collection of any useful, re-usable snippets, page layouts, functionalities and self-contained modules.

Idea is to create modular versions of stuff I've created on jobs - as reusabe units of code (or plugins)

### plan

html pages to showcase each of these might be added in kit files as page components



`_global-variables.kit` - using this to store content folder names, as they may change









