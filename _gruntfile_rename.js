module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //Copy html
    // Copy 
    copy: {
      html: {
        expand: true,
        src: '**/*.html',
        dest: 'build', 
        cwd: 'source'
      },
  
      assets: {
        expand: true,
        src: '**/*',
        dest: 'build/assets',
        cwd: 'source/files'
      },

      fontassets: {
        expand: true,
        src: '**/*',
        dest: 'build/assets/css/fonts',
        cwd: 'source/scss/base/fonts'
      },

      imgassets: {
        expand: true,
        src: '**/*',
        dest: 'build/assets/img',
        cwd: 'source/img'
      },

    },

    //Check JavaScript quality
    jshint: {
      all: ['source/js/components/**/*', 'source/js/helpers/**/*.js', 'source/js/scripts.js', '!source/js/components/group-content-scripts.js'],
                                                                                               //exclude js lifted from group site
      options: {
        jshintrc: '.jshintrc',
      }
    },

    //Code Kit / PrePros script append/prepend processing
    codekit: {
      kitFiles: {
        files: [{
          expand: true,
          cwd: 'source',
          src: ['*.kit'],
          dest: 'build',
          ext: '.html'
        }]
      },
      
      jsinclude : {
        files : {
          'build/assets/js/scripts.js' : 'source/js/scripts.js'
        }
      }
    },

    //Minify the JavaScript into the build folder
    uglify: {
      scripts: {
        files: {
          'build/assets/js/min/scripts.min.js' : ['build/assets/js/scripts.js']
        }
      }
    },

    //Lint the SCSS as per coding standards
    scsslint: {
      options: {
        config: '.scss-lint.yml',
        reporterOutput: 'scss-lint-report.xml',
        colorizeOutput: true,
      },

      allFiles: [ 'source/scss/**/*.scss', '!source/scss/**/vendor**/*.scss', '!source/scss/group-content/**/*.scss', '!source/scss/lint-ignore/**/*.scss' ]
                                                                              //exclude css lifted from group site     //exclude files set up to contain valid properties that scss-lint doesn't recognise
    },

    sass: {
      build: {
        options: {
          sourceMap: true,
          lineNumbers: true,
          outputStyle: 'expanded',
          require: 'susy'
        },

        files: {
          'build/assets/css/styles.css': 'source/scss/styles-global.scss',
          'build/assets/css/old-ie.css': 'source/scss/old-ie.scss'
        }
      },

      dist: {
        options: {
          outputStyle: 'compressed',
          require: 'susy'
        },
        files: {
          'build/assets/css/styles.css': 'source/scss/styles-global.scss',
          'build/assets/css/old-ie.css': 'source/scss/old-ie.scss'
        }
      }
    },

    // Autoprefixer
    autoprefixer: {
      dist: {
        options: {
          browsers: ['last 2 versions', 'ie 9']
        },
        files: {
          'build/assets/css/styles.css':'build/assets/css/styles.css',
        }
      }
    },

    //Connect server for running the pattern library
    connect: {
      server: {
        options: {
          hostname: '0.0.0.0',
          port: 8888,
          base: 'build',
          livereload: true
        }
      }
    },

    //Watch task with livereload
    watch: {
      html: {
        files: ['source/**/*.html', 'source/**/*.kit'],
        tasks: ['codekit:kitFiles','copy:html']
      },
      
      scripts: {
        files: ['source/js/**/*.js'],
        tasks: ['jshint','codekit:jsinclude']
      },

      styles: {
        files: 'source/scss/**/*.scss',
        tasks: ['sass', 'autoprefixer']
      }

      images: {
        files: ['source/img/**/*'],
        tasks: ['copy:imgassets']
      }

      livereload: {
        files: ['build/**/*'],
        options: {
          livereload: true
        }
      }
    }
    

  });

  // Load JavaScript quality check task.
  grunt.loadNpmTasks('grunt-contrib-jshint');

  // Load uglify task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Autoprefixer
  grunt.loadNpmTasks('grunt-autoprefixer');

  //Load SASS task
  grunt.loadNpmTasks('grunt-sass');

  //Load copy task
  grunt.loadNpmTasks('grunt-contrib-copy');

  //JavaScript append/prepend task
  grunt.loadNpmTasks('grunt-codekit');

  //Serve up the pattern library
  grunt.loadNpmTasks('grunt-contrib-connect');
  
  //SCSS lint
  grunt.loadNpmTasks('grunt-scss-lint');  

  //Watch task
  grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-watch-nospawn');

  //Load local grunt tasks
  // -- common-tasks.js - Common tasks across, pattern library, kitchen sink and build projects
  //grunt.loadTasks('./grunt-tasks');

  // Default task(s).
  grunt.registerTask('default', [
    'copy:html',
    'copy:assets',
    'copy:imgassets',
    'jshint',
    'codekit',
    'uglify',
    'scsslint',
    'sass:build',   
    'autoprefixer', 
    'connect:server',
    'watch'
  ]);

  grunt.registerTask('dist', [
    'copy:assets',
    'copy:imgassets',
    'jshint',
    'codekit',
    'uglify',
    'scsslint',
    'sass:dist',    
    'autoprefixer',
    'copy:dist'
  ]);
};