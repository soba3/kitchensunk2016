module.exports = function(grunt){
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //copy images and html to the build folder
    copy: {
      
      options: {
        timestamp: true
      },

      //copy over html + dependencies that aren't built with codekit (esp in /projects/)
      demos: {
        expand: true,
        cwd: 'src/html-pages',
        src: ['**/*.html', '**/*.css', '**/*.js', '**/*.json'],
        dest: 'build' //where all the dev blocks pages go
      },

      //copy the postCSS'd css to /build/ in watch 
      css: {
        src: 'src/scss/css/prefixed-all.css',
        dest: 'build/assets/css/all.css'
      },
      //copy the sass map to build
      sassmap: {
        src: 'src/scss/css/prefixed-all.css.map',
        dest: 'build/assets/css/all.css.map'
      },

      //all non-browserified js (but not the demos)
      js: {
        expand: true,
        cwd: 'src/js/',
        src: ['**/*', '!browserify/**/*'], //don't want all the component js for browserify!
        dest: 'build/assets/js/'
      },

      //all image assets
      images: {
        expand: true,
        cwd: 'src/img/',
        src: '**/*',
        dest: 'build/assets/img/'
      },

      //fonts (if self-hosted)
      fonts: {
        src: 'src/fonts/**/*',
        dest: 'build/assets/fonts/'
      }
      
      

      
      
    },


    //Codekit/prepros script append/prepend processing
    codekit: {
      kitFiles: {
        files: [{
          expand: true,
          cwd: 'src/html-pages/',
          src: ['**/*.kit'],
          dest: 'build',
          ext: '.html'
        }]
      }

      // jsinclude : {
      //   files : {
      //     'build/assets/js/scripts.js' : 'src/js/scripts.js'
      //   }
      // }
    },


    //compile CSS
    sass: {
      options: {
        sourceMap: true
        //For susy, need to install susy, then add it in as a sass task:
        // style: 'expanded',
        //require: 'susy'
      },
      dist: {
        src: 'src/scss/all.scss',
        dest: 'src/scss/css/all.css', //css file to be processed by postCSS 
      },
      //stylesheets for each module/project/demo
      separates: {
        expand: true,
        cwd: 'src/html-pages/',
        src: '**/*.scss',
        dest: 'build/',
        ext: '.css'
      }
    },
    
    //postCSS runs on the concatenated, compiled CSS file in /src/css/, 
    //and generates new (and final) css file in /build/css/
    postcss: {
      options: {
        map: true, // inline sourcemaps
        processors: [
          require('autoprefixer')({
            browsers: ['last 2 versions']  // add vendor prefixes
          })
          //require('pixrem')(), // add fallbacks for rem units (not yet installed)
        ]
      },
      dist: {
        src: 'src/scss/css/all.css', 
        dest: 'src/scss/css/prefixed-all.css' //autoprefix before minifying with cssmin
      },
      //stylesheets for each module/project/demo
      separates: {
        expand: true,
        cwd: 'build/',
        src: '**/*.css',
        dest: 'build/'
      }
    },

    //javascript tasks
    browserify: {
      'build/assets/js/browserify/app.js': 'src/js/browserify/main.js'
    },


    //server tasks:
    connect: {
      server: {
        options: {
          port: 8080,
          base: 'build/',
          livereload: true
        }
      }
    },

    watch: {
      options: {
        livereload: true
      },
      html: {
        files: ['src/**/*.html', 'src/**/*.kit'],
        tasks: ['codekit:kitFiles','copy:demos']
      },
      scss: {
        files: 'src/**/*.scss',
        tasks: ['sass', 'postcss', 'copy:css', 'copy:sassmap', 'copy:demos'],
        options: {}
      },
      apps: {
        files: ['src/**/*.js'],
        tasks: ['browserify', 'copy:js'],
        options: {
          interrupt: true,
        },
      },
      scripts: {
        files: ['src/html-pages/**/*.js', 'src/html-pages/**/*.json'],
        tasks: ['copy:demos'],
        options: {
          interrupt: true,
        },
      }
    },

  

    //minify js and css into the dist-build folder:
    uglify: {
      options: {
        report: 'gzip',
        sourceMap: true //don't need this, since only one file
      },
      target: {
        files: [{
                  'build/assets/js/browserify/app.js': ['src/js/browserify/app.js']
                },
                {
                  'build/assets/js/mobile-nav.js': ['src/js/browserify/mobile-nav.js']
                }

        ]
      }
    },
    cssmin: {
      target: {
        files: {
          'build/assets/css/all.css': ['src/scss/css/prefixed-all.css']
        }
      }
    },
    
    


  });
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-browserify');  
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-html-build');
  grunt.loadNpmTasks('grunt-codekit');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.registerTask('default', ['connect', 'watch']);
  grunt.registerTask('build', ['codekit', 'sass', 'postcss', 'browserify', 'uglify', 'cssmin', 'copy']);


};